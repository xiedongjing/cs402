package com.epoint.gshzwfw.parkcarioinfo.rest;

import com.epoint.common.api.ICommonService;
import com.epoint.common.util.JsonUtils;
import com.epoint.common.util.SqlConditionUtil;
import com.epoint.common.util.StringUtil;
import com.epoint.gshzwfw.parkcarioinfo.api.IParkcarIoinfoService;
import com.epoint.gshzwfw.parkcarioinfo.api.entity.ParkcarIoinfo;
import com.epoint.gshzwfw.parkcarioinfo.rest.dto.ParkcarIoinfoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Date;
import java.util.Objects;

/**
 * @author 谢东京
 * @descriptiion
 * @date 2021/8/23 15:59
 */
@RestController
@RequestMapping("/parkcar")
public class ParkcarIoinfoRestController {
    @Autowired
    private ICommonService commonService;
    @Autowired
    private IParkcarIoinfoService parkcarIoinfoService;
    /**
     * 日志
     */
    private Logger log = LoggerFactory.getLogger(ParkcarIoinfoDto.class);

    @PostMapping(value = "/ioInfo")
    public String createWorkorder(@RequestBody @Valid ParkcarIoinfoDto parkcarIoinfoDto) {
        String message = "";
        try{
            log.info("=========开始调用ioInfo方法=========， parkcarIoinfoDto：" ,parkcarIoinfoDto);
            validParkcarIoinfoDto(parkcarIoinfoDto);
            if(!"Epoint_WebSerivce_**##0601".equals(parkcarIoinfoDto.getApp_key())){
                return JsonUtils.zwdtRestReturn("0", "保存车辆进出信息失败，appkey无效", "");
            }
            ParkcarIoinfo parkcarIoinfo = new ParkcarIoinfo();
            parkcarIoinfo.setOperateusername("三方调用");
            parkcarIoinfo.setOperatedate(new Date());
            parkcarIoinfo.setRowguid(parkcarIoinfoDto.getRecord_id());
            parkcarIoinfo.setPark_id(parkcarIoinfoDto.getPark_id());
            parkcarIoinfo.setPark_name(parkcarIoinfoDto.getPark_name());
            parkcarIoinfo.setRecord_id(parkcarIoinfoDto.getRecord_id());
            parkcarIoinfo.setPlate_number(parkcarIoinfoDto.getPlate_number());
            parkcarIoinfo.setEntrance_time(new Date(parkcarIoinfoDto.getEntrance_time()));
            if(StringUtil.isNotBlank(parkcarIoinfoDto.getExit_time()) && parkcarIoinfoDto.getExit_time() != 0){
                parkcarIoinfo.setExit_time(new Date(parkcarIoinfoDto.getExit_time()));
            }
            parkcarIoinfo.setEntrancegate_id(parkcarIoinfoDto.getEntrancegate_id());
            parkcarIoinfo.setExitgate_id(parkcarIoinfoDto.getExitgate_id());
            parkcarIoinfo.setRelease_type(parkcarIoinfoDto.getRelease_type());
            parkcarIoinfo.setCartype_id(parkcarIoinfoDto.getCartype_id());
            parkcarIoinfo.setCarmodel_id(parkcarIoinfoDto.getCarmodel_id());
            parkcarIoinfo.setEnter_type(parkcarIoinfoDto.getEnter_type());
            parkcarIoinfo.setPlate_color(parkcarIoinfoDto.getPlate_color());
            parkcarIoinfo.setInimg_picture(parkcarIoinfoDto.getInimg_picture());
            parkcarIoinfo.setOutimg_picture(parkcarIoinfoDto.getOutimg_picture());
            parkcarIoinfo.setIs_exit(parkcarIoinfoDto.getIs_exit());
            parkcarIoinfo.setOwnername(parkcarIoinfoDto.getOwnername());
            //通过record_id判断该记录是否存在。
            SqlConditionUtil sqlConditionUtil = new SqlConditionUtil();
            sqlConditionUtil.eq("record_id", parkcarIoinfo.getRecord_id());
            ParkcarIoinfo oldParkcarIoinfo = commonService.getRecord(sqlConditionUtil.getMap(), ParkcarIoinfo.class).getResult();
            if(oldParkcarIoinfo != null){
                commonService.update(parkcarIoinfo);
                message = "更新车辆进出信息成功";
            }else{
                commonService.insert(parkcarIoinfo);
                message = "新增车辆进出信息成功";
            }
        }catch (Exception e){
            log.error("=============保存车辆进出信息失败，" + e.getMessage() + "===============");
            return JsonUtils.zwdtRestReturn("0", "保存车辆进出信息失败，" + e.getMessage(), "");
        }
        log.info("=========结束调用ioInfo方法============");
       return JsonUtils.zwdtRestReturn("1", message, "");
    }

    private void validParkcarIoinfoDto(ParkcarIoinfoDto parkcarIoinfoDto) throws Exception{
        if(StringUtil.isBlank(parkcarIoinfoDto.getApp_key())){
            throw new Exception("app_key不能为空");
        }
        if(StringUtil.isBlank(parkcarIoinfoDto.getPark_id())){
            throw new Exception("park_id(车厂唯一标识)不能为空");
        }
        if(StringUtil.isBlank(parkcarIoinfoDto.getPark_name())){
            throw new Exception("park_name(车厂名字)不能为空");
        }
        if(StringUtil.isBlank(parkcarIoinfoDto.getRecord_id())){
            throw new Exception("record_id(车辆入场时场库生成的唯一编号)不能为空");
        }
        if(StringUtil.isBlank(parkcarIoinfoDto.getPlate_number())){
            throw new Exception("plate_number(车牌号码)不能为空");
        }
        if(StringUtil.isBlank(parkcarIoinfoDto.getEntrance_time()) || parkcarIoinfoDto.getEntrance_time() == 0){
            throw new Exception("entrance_tiem(进场时间)不能为空或者为0");
        }
        if(StringUtil.isBlank(parkcarIoinfoDto.getEntrancegate_id())){
            throw new Exception("entrancegate_id(进场通道id)不能为空");
        }
        if(StringUtil.isBlank(parkcarIoinfoDto.getIs_exit())){
            throw new Exception("is_exit(是否出场)不能为空");
        }
    }


}
